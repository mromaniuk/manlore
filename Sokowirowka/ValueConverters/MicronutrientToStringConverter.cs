﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace Sokowirowka
{
    [ValueConversion(typeof(MicronutrientViewModel), typeof(string))]
    class MicronutrientToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            MicronutrientViewModel nutrient = (MicronutrientViewModel)value;
            string name = null;
            if (nutrient.name == micronutrient_type.vitaminA)
            {
                name = "Witamina A";
            }
            else if (nutrient.name == micronutrient_type.vitaminB)
            {
                name = "Witamina B";
            }
            else if (nutrient.name == micronutrient_type.vitaminC)
            {
                name = "Witamina C";
            }
            else if (nutrient.name == micronutrient_type.vitaminD)
            {
                name = "Witamina D";
            }
            return name + ": " + nutrient.amount.ToString() + "g";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}