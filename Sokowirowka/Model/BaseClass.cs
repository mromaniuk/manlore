﻿using System.Collections.Specialized;
using System.ComponentModel;

namespace Sokowirowka
{
    /// <summary>
    /// Base model that fires Property CHanged events as needed
    /// </summary>
    public class BaseClass : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}