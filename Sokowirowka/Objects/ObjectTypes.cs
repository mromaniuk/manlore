﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sokowirowka
{
    class nutrient
    {
        int amount;
    }

    class micronutrient : nutrient
    {
        enum micronutrient_type
        {
            vitaminA,
            vitaminB,
            vitaminC,
            vitaminD
        }

        micronutrient_type name;
    }

    class macronutrient : nutrient
    {
        enum macronutrient_type
        {
            proteins,
            carbohydrates,
            fats
        }

        macronutrient_type name;
    }

    class ingredient
    {
        enum ingredient_type
        {
            apple,
            banana,
            orange,
            lemon,
            strawberry,
            pineapple
        }

        ingredient_type name;
        List<nutrient> nutrients;
        int amount;
    }

    class juicer
    {
        enum model_type
        {
            Lite,
            Standard,
            Fruit_annihilator_3000
        }

        model_type model;
        int capacity;
        List<ingredient> contents;

    }
}
