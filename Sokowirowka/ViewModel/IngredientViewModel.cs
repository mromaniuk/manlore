﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sokowirowka
{
    public class IngredientViewModel : BaseViewModel
    {
        private ingredient_type _enumName;
        public ingredient_type enumName
        {
            get
            {
                return _enumName;
            }
            private set
            {
                _enumName = value;
            }
        }
        private int _amount;
        public int amount
        {
            get
            {
                return _amount;
            }
            set
            {
                _amount = value;
                UpdateLabel();
                OnPropertyChanged("amount");
            }
        }
        private string _displayName;
        public string displayName
        {
            get
            {
                return _displayName;
            }
            private set
            {
                _displayName = value;
                UpdateLabel();
            }
        }
        private string _ingredientLabel;
        public string ingredientLabel
        {
            get
            {
                return _ingredientLabel;
            }
            private set
            {
                _ingredientLabel = value;
                OnPropertyChanged("ingredientLabel");
            }
        }

        public IngredientViewModel(ingredient_type name, int amount, string displayName)
        {
            this.enumName = name;
            this.amount = amount;
            this.displayName = displayName;
        }

        private void UpdateLabel()
        {
            ingredientLabel = this.ToString();
        }
        public override string ToString()
        {
            return displayName + " " + amount.ToString() + "g";
        }
    }
}
