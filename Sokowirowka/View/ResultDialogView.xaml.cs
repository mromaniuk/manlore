﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sokowirowka
{
    /// <summary>
    /// Interaction logic for ResultDialogView.xaml
    /// </summary>
    public partial class ResultDialogView : Window
    {
        public ResultDialogView(JuicerViewModel source)
        {
            InitializeComponent();
            DataContext = source;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
