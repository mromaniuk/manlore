﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sokowirowka
{
    /// <summary>
    /// ViewModel ekranu startowego.
    /// </summary>
    public class StartScreenViewModel : BaseViewModel
    {
        public MainViewModel homeViewModel { get; private set; }

        private ObservableCollection<JuicerViewModel> _juicerCabinet;
        public ObservableCollection<JuicerViewModel> juicerCabinet
        {
            get
            {
                return _juicerCabinet;
            }
            private set
            {
                _juicerCabinet = value;
                OnPropertyChanged("juicer_cabinet");
            }
        }

        public StartScreenViewModel(ObservableCollection<JuicerViewModel> juicerCabinet, MainViewModel home)
        {
            homeViewModel = home;
            this.juicerCabinet = juicerCabinet;
            Console.WriteLine("podpięto szafkę z sokowirówkami do viewmodelu");
        }
    }
}
