﻿using Sokowirowka;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sokowirowka
{
    /// <summary>
    /// ViewModel panelu sokowirówki
    /// </summary>
    public class JuicerViewModel : BaseViewModel
    {
        public MainViewModel homeViewModel { get; set; }

        juicer chosenJuicer;

        public int capacity
        {
            get
            {
                return chosenJuicer.capacity;
            }
        }
        public int contentsWeight
        {
            get
            {
                return chosenJuicer.contents_weight;
            }
        }
        public model_type enumName
        {
            get
            {
                return chosenJuicer.model;
            }
        }
        public string juicerName { get; private set; }
        private ObservableCollection<IngredientViewModel> _contents = new ObservableCollection<IngredientViewModel>();
        public ObservableCollection<IngredientViewModel> contents
        {
            get
            {
                return _contents;
            }
            private set
            {
                _contents = value;
                OnPropertyChanged("contents");
            }
        }
        ObservableCollection<MacronutrientViewModel> _macronutrients = new ObservableCollection<MacronutrientViewModel>();
        public ObservableCollection<MacronutrientViewModel> macronutrients
        {
            get
            {
                return _macronutrients;
            }
            private set
            {
                _macronutrients = value;
            }
        }

        ObservableCollection<MicronutrientViewModel> _micronutrients = new ObservableCollection<MicronutrientViewModel>();
        public ObservableCollection<MicronutrientViewModel> micronutrients
        {
            get
            {
                return _micronutrients;
            }
            private set
            {
                _micronutrients = value;
            }
        }

        private int _basketIndex;
        private int _contentsIndex;
        public int basketIndex
        {
            get
            {
                return _basketIndex;
            }
            set
            {
                _basketIndex = value;
                OnPropertyChanged("basketIndex");
            }
        }
        public int contentsIndex
        {
            get
            {
                return _contentsIndex;
            }
            set
            {
                _contentsIndex = value;
                OnPropertyChanged("contentsIndex");
            }
        }

        private IngredientViewModel _chosenIngredient;
        public IngredientViewModel chosenIngredient
        {
            get
            {
                return _chosenIngredient;
            }
            set
            {
                if (value != null)
                {
                    _chosenIngredient = value;
                    amountOfChosenIngredient = _chosenIngredient.amount;

                    basketIndex = -1;
                    contentsIndex = -1;
                    OnPropertyChanged("contents");
                    OnPropertyChanged("chosenIngredient");
                }
            }
        }
        private string _amountOfChosenIngredientString;
        public string amountOfChosenIngredientString
        {
            get
            {
                return _amountOfChosenIngredientString;
            }
            set
            {
                _amountOfChosenIngredientString = value;
                int number;
                if(int.TryParse(value, out number))
                {
                    _amountOfChosenIngredient = number;
                }
                else
                {
                    Console.WriteLine("Nieprawidłowe dane");
                }
            }
        }
        private int _amountOfChosenIngredient;
        public int amountOfChosenIngredient
        {
            get
            {
                return _amountOfChosenIngredient;
            }
            private set
            {
                Console.WriteLine("Aktualizuję wagę");
                _amountOfChosenIngredient = value;
                _amountOfChosenIngredientString = value.ToString();
                OnPropertyChanged("amountOfChosenIngredientString");
            }
        }
        private ResultDialogView resultWindow;

        public ICommand manipulateContent { get; private set; }
        public ICommand squeezeOrEmpty { get; private set; }

        public JuicerViewModel(juicer chosen_juicer, MainViewModel mainViewModel)
        {
            Console.WriteLine("Stworzono viewmodel sokowirowki");

            this.chosenJuicer = chosen_juicer;
            this.chosenJuicer.PropertyChanged += Model_PropertyChanged;
            this.juicerName = AssignName();
            this.homeViewModel = mainViewModel;
            manipulateContent = new ManipulateContentCommand(this);
            squeezeOrEmpty = new SqueezeOrEmptyCommand(this);
        }

        private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "contents")
            {
                UpdateTank();
            }
        }

        private void UpdateTank()
        {
            //this.contents = new ObservableCollection<IngredientViewModel>();
            this.contents.Clear();
            foreach ( ingredient fruit in this.chosenJuicer.contents )
            {
                this.contents.Add(new IngredientViewModel(fruit._name, fruit.weight, fruit.wordName));
            }
            OnPropertyChanged("contentsWeight");
        }

        public void AddIngredient()
        {
            this.chosenJuicer.AddIngredient(chosenIngredient.enumName, ref _amountOfChosenIngredient);
            Analyze();
        }

        public void RemoveIngredient()
        {
            if (chosenIngredient != null)
            {
                this.chosenJuicer.RemoveIngredient(chosenIngredient.enumName, ref _amountOfChosenIngredient);
            }Analyze();
        }
    
        public void Analyze()
        {
            
            var macronutrients_types = Enum.GetValues(typeof(macronutrient_type));
            var micronutrients_types = Enum.GetValues(typeof(micronutrient_type));
            macronutrients.Clear();
            micronutrients.Clear();

            if (chosenJuicer.contents.Count != 0)
            {
                foreach (macronutrient_type macronutrient in macronutrients_types)
                {
                    macronutrients.Add(new MacronutrientViewModel(macronutrient, this.chosenJuicer.CalculateMacronutrients(macronutrient)));
                }
                foreach (micronutrient_type micronutrient in micronutrients_types)
                {
                    micronutrients.Add(new MicronutrientViewModel(micronutrient, this.chosenJuicer.CalculateMicronutrients(micronutrient)));
                }
            }
        }

        public void Squeeze()
        {
            Analyze();
            resultWindow = new ResultDialogView(this);
            resultWindow.ShowDialog();
            EmptyTank();
        }

        public void EmptyTank()
        {
            this.chosenJuicer.EmptyTank();
            macronutrients.Clear();
            micronutrients.Clear();
        }

        private string AssignName()
        {
            Console.WriteLine("Odczytano sokowirowke");
            if ( chosenJuicer.model == model_type.Lite )
            {
                Console.WriteLine("Lite");
                return "Lite";
            }
            else if ( chosenJuicer.model == model_type.Standard )
            {
                Console.WriteLine("Standard");
                return "Standard";
            }
            else if ( chosenJuicer.model == model_type.Fruit_annihilator_3000 )
            {
                Console.WriteLine("Turbo");
                return "Fruit annihilator 3000";
            }
            else
            {
                Console.WriteLine("ERRORERRORERROR");
                return "eRrOr!!!!!";
            }
        }

        public override void Clear()
        {
            EmptyTank();
            _chosenIngredient = null;
        }
    }
}
