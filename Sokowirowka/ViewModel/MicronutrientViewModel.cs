﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sokowirowka
{
    public class MicronutrientViewModel
    {
        public micronutrient_type name
        {
            get;
            private set;
        }
        public double amount
        {
            get;
            private set;
        }

        public MicronutrientViewModel(micronutrient_type name, double amount)
        {
            this.name = name;
            this.amount = amount;
        }

        public override string ToString()
        {
            return name.ToString() + " " + amount.ToString() + "g";
        }
    }
}