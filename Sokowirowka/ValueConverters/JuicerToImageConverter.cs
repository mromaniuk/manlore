﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace Sokowirowka
{
    [ValueConversion(typeof(model_type), typeof(BitmapImage))]
    class JuicerToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string myPath = "/Icons/steak.png";
            model_type juicer = (model_type)value;
            try
            {
                if (juicer == model_type.Lite)
                {
                    myPath = "/Icons/feather.png";
                }
                else if (juicer == model_type.Standard)
                {
                    myPath = "/Icons/juice.png";
                }
                else if (juicer == model_type.Fruit_annihilator_3000)
                {
                    myPath = "/Icons/skull.png";
                }

                Uri myURI = new Uri(myPath, UriKind.RelativeOrAbsolute);
                BitmapImage anImage = new BitmapImage(myURI);
                return anImage;
            }
            catch (Exception)
            {
                Console.WriteLine("Image not found");
                myPath = "/Icons/steak.png";

                Uri myURI = new Uri(myPath, UriKind.RelativeOrAbsolute);
                BitmapImage anImage = new BitmapImage(myURI);
                return anImage;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}