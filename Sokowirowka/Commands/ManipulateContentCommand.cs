﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sokowirowka
{
    public class ManipulateContentCommand : ICommand
    {
        JuicerViewModel source;
        int number;

        public ManipulateContentCommand(JuicerViewModel source)
        {
            this.source = source;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            if (source.chosenIngredient != null && int.TryParse(source.amountOfChosenIngredientString, out number) && number >= 0)
                return true;
            else
                return false;
        }

        public void Execute(object parameter)
        {
            if(parameter.ToString() == "Add")
            {
                source.AddIngredient();
            }
            else if(parameter.ToString() == "Remove")
            {
                source.RemoveIngredient();
            }
        }
    }
}
