﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace Sokowirowka
{
    [ValueConversion(typeof(MacronutrientViewModel), typeof(string))]
    class MacronutrientToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            MacronutrientViewModel nutrient = (MacronutrientViewModel)value;
            string name = null;
            if (nutrient.name == macronutrient_type.proteins)
            {
                name = "Białko";
            }
            else if (nutrient.name == macronutrient_type.carbohydrates)
            {
                name = "Węglowodany";
            }
            else if (nutrient.name == macronutrient_type.fats)
            {
                name = "Tłuszcz";
            }
            return name + ": " + nutrient.amount.ToString() + "g";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}