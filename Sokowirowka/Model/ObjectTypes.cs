﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sokowirowka
{
    public abstract class Nutrient
    {
        public double weight { get; protected set; }
    }


    public enum micronutrient_type
    {
        vitaminA,
        vitaminB,
        vitaminC,
        vitaminD
    }

    public class micronutrient : Nutrient
    {
        public micronutrient_type _name { get; private set; }

        public micronutrient(micronutrient_type _name, double amount)
        {
            this._name = _name;
            weight = amount;
        }
    }


    public enum macronutrient_type
    {
        proteins,
        carbohydrates,
        fats
    }

    public class macronutrient : Nutrient
    {
        public macronutrient_type _name { get; private set; }

        public macronutrient(macronutrient_type _name, double amount)
        {
            this._name = _name;
            weight = amount;
        }
    }

    public enum ingredient_type
    {
        apple,
        banana,
        orange,
        lemon,
        strawberry,
        pineapple
    }

    /// <summary>
    /// Klasa owocu. Owoc jest rozpoznawalny przez każdego, nie jest możliwa zmiana jego typu ani składu.
    /// </summary>
    public class ingredient : BaseClass
    {
        public ingredient_type _name
        {
            get;
            private set;
        }
        private string _wordName;
        public string wordName
        {
            get
            {
                return _wordName;
            }
            set
            {
                _wordName = value;
            }
        }
        private List<macronutrient> macronutrients;
        private List<micronutrient> micronutrients;
        public int weight { get; set; }


        /// <summary>
        /// Konstruktor kopiujący z przekazaniem przez referencję. Do użycia przy dodawaniu poprzednio nieobecnego w zbiorniku owocu
        /// </summary>
        /// <param name="ingredient_to_clone">Obiekt do sklonowania</param>
        /// <param name="_weight">Ilość składnika do sklonowania</param>
        public ingredient(ingredient ingredient_to_clone, ref int _weight)
        {
            _name = ingredient_to_clone._name;
            weight = _weight;
            macronutrients = ingredient_to_clone.macronutrients;
            micronutrients = ingredient_to_clone.micronutrients;
            wordName = ingredient_to_clone.wordName;
        }

        /// <summary>
        /// Zwykły konstruktor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="macronutrients"></param>
        /// <param name="micronutrients"></param>
        /// <param name="weight"></param>
        public ingredient(ingredient_type name, List<macronutrient> macronutrients, List<micronutrient> micronutrients, int weight, string wordName)
        {
            _name = name;
            this.macronutrients = macronutrients;
            this.micronutrients = micronutrients;
            this.weight = weight;
            this.wordName = wordName;
        }

        public double calculate_macronutrients(macronutrient_type calculated_type)
        {
            double amount = 0;
            foreach(macronutrient _nutrient in macronutrients)
            {
                if (_nutrient._name == calculated_type)
                {
                    amount = _nutrient.weight / 100 * weight;
                    break;
                }
            }
            return amount;
        }
        public double calculate_micronutrients(micronutrient_type calculated_type)
        {
            double amount = 0;
            foreach (micronutrient _nutrient in micronutrients)
            {
                if (_nutrient._name == calculated_type)
                {
                    amount = _nutrient.weight / 100 * weight;
                    break;
                }
            }
            return amount;
        }
    }

    public enum model_type
    {
        Lite,
        Standard,
        Fruit_annihilator_3000
    }

    /// <summary>
    /// Sokowirówka. Ma plakietkę z nazwą, którą każdy może przeczytać ale nie da się jej zmienić. Podobnie z pojemnością oraz z wagą zawartości.
    /// </summary>
    public class juicer : BaseClass
    {
        public model_type model;
        private int _capacity;
        public int capacity
        {
            get
            {
                return _capacity;
            }
            private set
            {
                _capacity = value;
                OnPropertyChanged("capacity");
            }
        }
        private int _contents_weight;
        public int contents_weight
        {
            get
            {
                return _contents_weight;
            }
            private set
            {
                _contents_weight = value;
                OnPropertyChanged("contentsWeight");
            }
        }
        private List<ingredient> _contents = new List<ingredient>();
        public List<ingredient> contents
        {
            get
            {
                return _contents;
            }
            private set
            {
                _contents = value;
            }
        }
        private Table table;

        public juicer(model_type model, int capacity, Table table)
        {
            Console.WriteLine("Stworzono sokowirówkę");
            this.model = model;
            this.capacity = capacity;
            this.contents_weight = 0;
            this.table = table;
        }

        public double CalculateMacronutrients(macronutrient_type calculated_type)
        {
            double nutrient_amount = 0;
            foreach (ingredient fruit in contents)
            {
                nutrient_amount += fruit.calculate_macronutrients(calculated_type);
            }
            return nutrient_amount;
        }

        public double CalculateMicronutrients(micronutrient_type calculated_type)
        {
            double nutrient_amount = 0;
            foreach (ingredient fruit in contents)
            {
                nutrient_amount += fruit.calculate_micronutrients(calculated_type);
            }
            return nutrient_amount;
        }

        private void CalculateContentsWeight()
        {
            contents_weight = 0;
            foreach(ingredient _ingredient in contents)
            {
                contents_weight += _ingredient.weight;
            }
        }

        /// <summary>
        /// Dodaje składnik w podanej ilości do zbiornika
        /// </summary>
        /// <param name="_ingredient">Typ składnika do dodania</param>
        /// <param name="amount">Ilość składnika do dodania</param>
        public void AddIngredient(ingredient_type _ingredient, ref int amount)
        {
            int to_add = amount;
            if (capacity < contents_weight + amount)
                to_add = capacity - contents_weight;
            else
                to_add = amount;

            if (to_add > 0)
            {
                ingredient added_ingredient = contents.SingleOrDefault(x => x._name == _ingredient);
                if (added_ingredient != null)
                {
                    added_ingredient.weight += to_add;
                }
                else
                {
                    ingredient from_basket = table.fruit_basket.SingleOrDefault(x => x._name == _ingredient);
                    if (from_basket != null)
                    {
                        contents.Add(new ingredient(from_basket, ref to_add));
                    }
                }
                CalculateContentsWeight();
                OnPropertyChanged("contents");
            }
            return;
        }
        public void RemoveIngredient(ingredient_type _ingredient, ref int amount)
        {
            ingredient deleted_ingredient = contents.FirstOrDefault(en => en._name == _ingredient);
            if (deleted_ingredient != null)
            {
                deleted_ingredient.weight -= amount;
                if (deleted_ingredient.weight <= 0)
                {
                    contents.Remove(deleted_ingredient);
                }
                CalculateContentsWeight();
                OnPropertyChanged("contents");
            }
        }

        public void EmptyTank()
        {
            contents = new List<ingredient>();
            CalculateContentsWeight();
            OnPropertyChanged("contents");
        }
    }

    /// <summary>
    /// Kuchenny blat. Ma szafkę z sokowirówkami oraz koszyk z owocami. Każdy może operować tymi kontenerami
    /// </summary>
    public class Table
    {
        public List<juicer> juicers_cabinet = new List<juicer>();
        public List<ingredient> fruit_basket = new List<ingredient>();

        public Table()
        {
            juicers_cabinet.Add(new juicer(model_type.Lite, 500, this));
            juicers_cabinet.Add(new juicer(model_type.Standard, 1000, this));
            juicers_cabinet.Add(new juicer(model_type.Fruit_annihilator_3000, 1500, this));

            macronutrient proteins = new macronutrient(macronutrient_type.proteins, 0.27);
            macronutrient carbohydrates = new macronutrient(macronutrient_type.carbohydrates, 13);
            macronutrient fats = new macronutrient(macronutrient_type.fats, 0.13);
            micronutrient vitaminA = new micronutrient(micronutrient_type.vitaminA, 0);
            micronutrient vitaminB = new micronutrient(micronutrient_type.vitaminB, 0.000155);
            micronutrient vitaminC = new micronutrient(micronutrient_type.vitaminC, 0.004);
            micronutrient vitaminD = new micronutrient(micronutrient_type.vitaminD, 0);

            fruit_basket.Add(new ingredient( ingredient_type.apple, new List<macronutrient> { proteins, carbohydrates, fats }, new List<micronutrient> { vitaminA, vitaminB, vitaminC, vitaminD }, 60, "Jabłko"));

            proteins = new macronutrient(macronutrient_type.proteins, 1.1);
            carbohydrates = new macronutrient(macronutrient_type.carbohydrates, 23);
            fats = new macronutrient(macronutrient_type.fats, 0.33);
            vitaminA = new micronutrient(micronutrient_type.vitaminA, 0);
            vitaminB = new micronutrient(micronutrient_type.vitaminB, 0.000804);
            vitaminC = new micronutrient(micronutrient_type.vitaminC, 0.0087);
            vitaminD = new micronutrient(micronutrient_type.vitaminD, 0);

            fruit_basket.Add(new ingredient(ingredient_type.banana, new List<macronutrient> { proteins, carbohydrates, fats }, new List<micronutrient> { vitaminA, vitaminB, vitaminC, vitaminD }, 70, "Banan"));

            proteins = new macronutrient(macronutrient_type.proteins, 1.1);
            carbohydrates = new macronutrient(macronutrient_type.carbohydrates, 9.3);
            fats = new macronutrient(macronutrient_type.fats, 0.3);
            vitaminA = new micronutrient(micronutrient_type.vitaminA, 0);
            vitaminB = new micronutrient(micronutrient_type.vitaminB, 0.00033);
            vitaminC = new micronutrient(micronutrient_type.vitaminC, 0.053);
            vitaminD = new micronutrient(micronutrient_type.vitaminD, 0);

            fruit_basket.Add(new ingredient(ingredient_type.lemon, new List<macronutrient> { proteins, carbohydrates, fats }, new List<micronutrient> { vitaminA, vitaminB, vitaminC, vitaminD }, 50, "Cytryna"));
            
            proteins = new macronutrient(macronutrient_type.proteins, 0.94);
            carbohydrates = new macronutrient(macronutrient_type.carbohydrates, 11.75);
            fats = new macronutrient(macronutrient_type.fats, 0.12);
            vitaminA = new micronutrient(micronutrient_type.vitaminA, 0);
            vitaminB = new micronutrient(micronutrient_type.vitaminB, 0.000187);
            vitaminC = new micronutrient(micronutrient_type.vitaminC, 0.0532);
            vitaminD = new micronutrient(micronutrient_type.vitaminD, 0);

            fruit_basket.Add(new ingredient(ingredient_type.orange, new List<macronutrient> { proteins, carbohydrates, fats }, new List<micronutrient> { vitaminA, vitaminB, vitaminC, vitaminD }, 150, "Pomarańcza"));
        }
    }
}