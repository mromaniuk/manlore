﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sokowirowka
{
    public class SqueezeOrEmptyCommand : ICommand
    {
        JuicerViewModel source;

        public SqueezeOrEmptyCommand(JuicerViewModel source)
        {
            this.source = source;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            if (source.contents.Count() == 0)
                return false;
            else
                return true;
        }

        public void Execute(object parameter)
        {
            if (parameter.ToString() == "Empty")
            {
                source.EmptyTank();
            }
            else if (parameter.ToString() == "Squeeze")
            {
                source.Squeeze();
            }
        }
    }
}
