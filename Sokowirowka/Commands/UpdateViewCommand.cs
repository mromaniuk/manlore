﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sokowirowka
{
    public class UpdateViewCommand : ICommand
    {
        private MainViewModel viewModel;

        public UpdateViewCommand(MainViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            if (viewModel.juicerToBeSelected == null)
            {
                return false;
            }
            return true;
        }

        public void Execute(object parameter)
        {
            Console.WriteLine("Opening chosen View Model");

            if (parameter.ToString() == "Home")
            {
                viewModel.selectedViewModel.Clear();
                viewModel.selectedViewModel = viewModel.mainMenu;
            }
            else if (parameter.ToString() == "Juicer")
                viewModel.selectedViewModel = viewModel.juicerToBeSelected;
        }
    }
}
