﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace Sokowirowka
{
    [ValueConversion(typeof(ingredient_type), typeof(BitmapImage))]
    class IngredientToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string myPath = "/Icons/steak.png";
            ingredient_type ingredient = (ingredient_type)value;
            try
            {
                if (ingredient == ingredient_type.apple)
                {
                    myPath = "/Icons/apple-1.png";
                }
                else if (ingredient == ingredient_type.banana)
                {
                    myPath = "/Icons/banana.png";
                }
                else if (ingredient == ingredient_type.lemon)
                {
                    myPath = "/Icons/lemon-1k.png";
                    Console.WriteLine("Cytryna");
                }
                else if (ingredient == ingredient_type.orange)
                {
                    myPath = "/Icons/orange.png";
                }
                else if (ingredient == ingredient_type.pineapple)
                {
                    myPath = "/Icons/pineapple.png";
                }
                else if (ingredient == ingredient_type.strawberry)
                {
                    myPath = "/Icons/strawberry.png";
                }

                Uri myURI = new Uri(myPath, UriKind.RelativeOrAbsolute);
                BitmapImage anImage = new BitmapImage(myURI);
                return anImage;
            }
            catch (Exception)
            {
                Console.WriteLine("Image not found");
                myPath = "/Icons/steak.png";

                Uri myURI = new Uri(myPath, UriKind.RelativeOrAbsolute);
                BitmapImage anImage = new BitmapImage(myURI);
                return anImage;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
