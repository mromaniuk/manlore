﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sokowirowka
{
    /// <summary>
    /// Główna klasa ViewModelu. W jej obrębie wyświetlane są wszystkie okna
    /// </summary>
    public class MainViewModel: BaseViewModel
    {

        private BaseViewModel _selectedViewModel;

        private ObservableCollection<JuicerViewModel> _listOfJuicerViewModels = new ObservableCollection<JuicerViewModel>();
        private ObservableCollection<IngredientViewModel> listOfIngredientViewModels = new ObservableCollection<IngredientViewModel>();
        public ObservableCollection<IngredientViewModel> _listOfIngredientViewModels
        {
            get
            {
                return listOfIngredientViewModels;
            }
            private set
            {
                listOfIngredientViewModels = value;
            }
        }
        private JuicerViewModel _juicerToBeSelected;
        public JuicerViewModel juicerToBeSelected
        {
            get
            {
                return _juicerToBeSelected;
            }
            set
            {
                _juicerToBeSelected = value;
            }
        }

        public StartScreenViewModel mainMenu;
        Table table;
        public BaseViewModel selectedViewModel
        {
            get { return _selectedViewModel; }
            set
            {
                _selectedViewModel = value;
                OnPropertyChanged(nameof(selectedViewModel));
            }
        }

        public ICommand updateViewCommand { get; private set; }

        public MainViewModel()
        {
            updateViewCommand = new UpdateViewCommand(this);


            table = new Table();

            foreach (ingredient _ingredient in table.fruit_basket)
            {
                _listOfIngredientViewModels.Add(new IngredientViewModel(_ingredient._name, _ingredient.weight, _ingredient.wordName));
            }
            foreach (juicer _juicer in table.juicers_cabinet)
            {
                _listOfJuicerViewModels.Add(new JuicerViewModel(_juicer, this));
            }
            mainMenu = new StartScreenViewModel(_listOfJuicerViewModels, this);

            _selectedViewModel = mainMenu;
        }
    }
}
